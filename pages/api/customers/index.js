import { Customers } from "../../../utility/data/Customers";

export default function handler(req, res) {
  if (req.method === "GET") {
    res.status(200).send({ data: Customers });
  } else if (req.method === "POST") {
    const email = req.body.email;
    if (email.length === 0) {
      res.status(400).send({ message: "Email cannot empty" });
    } else {
      const newEmail = {
        id: Date.now(),
        email: email,
      };
      Customers.push(newEmail);
      res.status(201).send({ data: newEmail });
    }
  }
}
