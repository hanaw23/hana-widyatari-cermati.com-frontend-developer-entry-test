import Head from "next/head";
import HeroHeader from "../components/headers/HeroHeader";
import NotifModal from "../components/modals/NotifModal";
import HighlightGrid from "../components/grids/HighlightGrid";
import Footer from "../components/footers/Footer";
import NewsletterModal from "../components/modals/NewsletterModal";

export default function Home() {
  return (
    <>
      <Head>
        <title>Hana Widyatari | Cermati.com Front-end Developer Entry Test</title>
      </Head>

      <div className="h-full overflow-x-hidden">
        <NewsletterModal />
        <NotifModal />
        <HeroHeader />
        <HighlightGrid />
        <Footer />
      </div>
    </>
  );
}
