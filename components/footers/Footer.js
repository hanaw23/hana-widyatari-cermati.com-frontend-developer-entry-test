export default function Footer() {
  return (
    <div className="w-full flex justify-center bg-[#050327] h-[80px]">
      <div className="py-8">
        <p className="text-white text-xs font-light">© 2022 Hana Widyatari. All rights reserved.</p>
      </div>
    </div>
  );
}
