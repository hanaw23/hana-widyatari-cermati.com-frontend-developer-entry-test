import CloseIcon from "../svg/CloseIcon";

export default function CloseButton(props) {
  return (
    <div>
      <button onClick={props.closeAction}>
        <CloseIcon height={15} width={15} className="fill-white" />
      </button>
    </div>
  );
}
