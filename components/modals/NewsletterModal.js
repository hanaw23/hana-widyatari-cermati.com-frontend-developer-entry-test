import ReactDOM from "react-dom";
import { useEffect, useState } from "react";

import CloseButton from "../buttons/CloseButton";
import styles from "./Modal.module.css";

export default function NewsletterModal() {
  const [email, setEmail] = useState("");

  const [closeUpdate, setCloseUpdate] = useState(false);
  const [isBrowser, setIsBrowser] = useState(false);
  const [isWindow, setIsWindow] = useState(false);
  const [isSucces, setIsSuccess] = useState(false);
  const [isFailed, setisFailed] = useState(false);
  const [failedMessage, setFailedMessage] = useState("");

  useEffect(() => {
    setIsBrowser(true);
    setIsWindow(true);
  }, []);

  const handleCloseUpdateModal = () => {
    setCloseUpdate(true);

    setTimeout(() => {
      setCloseUpdate(false);
    }, 600000);
  };

  const handleCloseUpdateModalIsSuccess = () => {
    setCloseUpdate(true);
  };

  const handleCloseUpdateModalIsFailed = () => {
    setCloseUpdate(true);

    setTimeout(() => {
      setCloseUpdate(false);
    }, 100);

    setisFailed(false);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const submitEmail = async () => {
    try {
      const response = await fetch("/api/customers", {
        method: "POST",
        body: JSON.stringify({ email }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.status === 201) {
        setIsSuccess(true);
      } else if (response.status === 400) {
        setisFailed(true);
        setFailedMessage("We Cannot reach you. Email cannot Empty ! Please fill the form before you click 'Count Me!' button");
      }
    } catch (error) {
      setisFailed(true);
      setFailedMessage(error);
    }
  };

  const cardModal = (
    <>
      {isSucces ? (
        <>
          <div className="flex justify-end -mr-3">
            <CloseButton closeAction={handleCloseUpdateModalIsSuccess} />
          </div>
          <div className="text-white">
            <h1 className=" text-[30px] font-semibold mb-2">Success Add Your Email</h1>
            <p>Thank you for filled in form. I will contact you for updates about web technologies through email. Keep healty and happy!</p>
          </div>
        </>
      ) : isFailed ? (
        <>
          <div className="flex justify-end -mr-3">
            <CloseButton closeAction={handleCloseUpdateModalIsFailed} />
          </div>
          <div className="text-white">
            <h1 className=" text-[30px] font-semibold mb-2">ERROR ! Add Your Email </h1>
            <p>{failedMessage}</p>
          </div>
        </>
      ) : (
        <>
          <div className="flex justify-end -mr-3">
            <CloseButton closeAction={handleCloseUpdateModal} />
          </div>
          <div className="text-white">
            <h1 className=" text-[30px] font-semibold mb-2">Get latest updates in web technologies</h1>
            <p>I write articles related to web technologies, such as design trends, development tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get them all.</p>
          </div>
          <div className="mt-6 flex flex-col desktop:flex-row laptop:flex-row tablet:flex-row gap-3">
            <input className="w-full h-[30px] px-2 placeholder:text-sm outline-none focus:outline-[#75c6f9]" type="email" placeholder="Email address" onChange={handleEmailChange} value={email} />
            <button onClick={submitEmail} type="submit" className="border border-transparent bg-[#FBA200] rounded px-2 text-white font-semibold hover:text-white hover:bg-transparent desktop:w-56 laptop:w-56 tablet:w-40">
              Count me in!
            </button>
          </div>
        </>
      )}
    </>
  );

  const modalResponsive = (
    <>
      {isWindow ? (
        window.innerWidth <= 800 ? (
          <div
            className={` ${styles.slideInMobile} ${styles.animated} ${styles.animated.faster} bg-[#1681c3ee] w-full desktop:w-[640px] laptop:w-[640px] tablet:w-[640px] desktop:h-[250px] laptop:h-[250px] tablet:h-[250px] px-8 py-3 z-10 -mb-20 fixed`}
          >
            {cardModal}
          </div>
        ) : window.innerWidth <= 1000 ? (
          <div
            className={` ${styles.slideInTablet} ${styles.animated} ${styles.animated.faster} bg-[#1681c3ee] w-full desktop:w-[640px] laptop:w-[640px] tablet:w-[640px] desktop:h-[250px] laptop:h-[250px] tablet:h-[250px] px-8 py-3 z-10 -mb-20 fixed`}
          >
            {cardModal}
          </div>
        ) : window.innerWidth <= 490 ? (
          <div
            className={`${styles.slideInSmallMobile} ${styles.animated} ${styles.animated.faster} bg-[#1681c3ee] w-full desktop:w-[640px] laptop:w-[640px] tablet:w-[640px] desktop:h-[250px] laptop:h-[250px] tablet:h-[250px] px-8 py-3 z-10 -mb-20 fixed`}
          >
            {cardModal}
          </div>
        ) : (
          <div
            className={`${styles.slideInDesktop} ${styles.animated} ${styles.animated.faster} bg-[#1681c3ee] w-full desktop:w-[640px] laptop:w-[640px] tablet:w-[640px] desktop:h-[250px] laptop:h-[250px] tablet:h-[250px] px-8 py-3 z-10 -mb-20 fixed`}
          >
            {cardModal}
          </div>
        )
      ) : null}
    </>
  );

  const modalShow = <>{closeUpdate ? null : modalResponsive}</>;

  if (isBrowser) {
    return ReactDOM.createPortal(modalShow, document.body);
  } else {
    return null;
  }
}
