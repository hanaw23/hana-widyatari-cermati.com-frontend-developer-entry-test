import { useState } from "react";

import styles from "./Modal.module.css";

export default function NotifModal() {
  const [closeNotif, setCloseNotif] = useState(false);

  const handleCloseNotif = () => {
    setCloseNotif(true);
  };

  const message = (text) => {
    switch (text) {
      case "Privacy Policy":
        return <button className="text-blue-600 ">Privacy Policy</button>;

      case "Terms of Service":
        return <button className="text-blue-600">Terms of Service</button>;

      case "Cookie Policy":
        return <button className="text-blue-600 ">Cookie Policy</button>;

      default:
        break;
    }
  };

  return (
    <div
      className={`bg-white w-full h-[150px] desktop:h-[75px] laptop:h-[75px] tablet:h-[75px] ${closeNotif ? `${styles.slideUp}` : "fixed"} ${closeNotif ? "desktop:-mt-[100px]" : "desktop:mt-0"} ${
        closeNotif ? "laptop:-mt-[100px]" : "laptop:mt-0"
      } ${closeNotif ? "tablet:-mt-[100px]" : "tablet:mt-0"} ${closeNotif ? "-mt-[300px]" : "mt-0"}`}
    >
      <div className="flex flex-col desktop:flex-row laptop:flex-row tablet:flex-row justify-center gap-2 tablet:gap-6 laptop:gap-6 desktop:gap-6 mt-4 ml-6 desktop:ml-0">
        <div className="w-[300px] desktop:w-[500px] laptop:w-[500px] tablet:w-[500px] text-sm">
          <span>By accessing and using this website, you acknowledge that you have read and understand our</span> {message("Cookie Policy")}
          <span>, </span> {message("Privacy Policy")}
          <span>, and our </span> {message("Terms of Service")}
          <span>.</span>
        </div>

        <button className="text-sm border border-none bg-[#007bc1] text-white rounded w-[50px] desktop:w-[70px] py-[5px]" onClick={handleCloseNotif}>
          Got it
        </button>
      </div>
    </div>
  );
}
