import styles from "./Hero.module.css";

export default function HeroHeader() {
  return (
    <div className={`${styles.background} w-full h-[700px] desktop:h-[575px] laptop:h-[575px] tablet:h-[575px] flex flex-col text-center`}>
      <div className="mt-[100px]">
        <img src="assets/y-logo-white.png" alt="logo" width={40} height={40} className="ml-8 mt-20 tablet:mt-4 laptop:mt-4 desktop:mt-4" />
        <div className="mt-[50px]">
          <h1 className="text-white text-[45px]">Hello! I'm Hana Widyatari</h1>
          <h1 className="text-white text-[25px] font-semibold">Consult, Design, and Develop Websites</h1>
          <div className="text-white text-[15px] mt-5">
            <p>Have something great in mind? Feel free to contact me.</p>
            <p>I'll help you to make it happen.</p>
          </div>
          <button className=" text-white mt-10 border border-white rounded w-fit hover:bg-white hover:text-[#004a75] font-semibold">
            <p className="text-sm py-3 px-3">LET'S MAKE CONTACT</p>
          </button>
        </div>
      </div>
    </div>
  );
}
