export default function GridHeader() {
  return (
    <div className="w-full m-auto">
      <div className="w-[300px] desktop:w-[600px] laptop:w-[600px] tablet:w-[600px] m-auto">
        <h1 className=" text-[30px] laptop:text-[45px] tablet:text-[45px] desktop:text-[45px]">How Can I Help You?</h1>

        <p className="mt-4">Our work then targeted, best practices outcomes social innovation synergy.</p>
        <p>Venture philanthropy, revolutionary inclusive policymaker relief. User-centered program areas scale.</p>
      </div>
    </div>
  );
}
