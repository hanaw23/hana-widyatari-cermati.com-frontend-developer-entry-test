import GridHeader from "../headers/GridHeader";
import { highlightPanel } from "../../utility/HighlightPanel";

export default function HighlightGrid() {
  return (
    <div className="w-full laptop:w-full desktop:w-full tablet:w-full mb-20">
      <div className="mt-20 text-center">
        <GridHeader />
      </div>
      <div className="mt-16 m-auto">
        <div className="flex justify-center">
          <div className="grid grid-cols-1 tablet:grid-cols-2 laptop:grid-cols-3 desktop:grid-cols-3 gap-10 tablet:gap-10 laptop:gap-10 desktop:gap-20">
            {highlightPanel.map((item, i) => (
              <div key={i} className="w-[300px] desktop:w-[400px] laptop:w-[300px] tablet:w-[400px] border border-[#ACAFA9] px-4 py-4">
                <div className="flex justify-between">
                  <h1 className="text-[25px] mb-4">{item.title}</h1>
                  {item.icon}
                </div>
                <p className="text-sm">{item.desc}</p>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
