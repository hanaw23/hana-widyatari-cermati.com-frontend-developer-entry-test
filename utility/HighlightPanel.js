import ConsultIcon from "../components/svg/ConsultIcon";
import DesignIcon from "../components/svg/DesignIcon";
import DevelopIcon from "../components/svg/DevelopIcon";
import MarketingIcon from "../components/svg/MarketingIcon";
import EvolveIcon from "../components/svg/EvolveIcon";
import ManageIcon from "../components/svg/ManageIcon";

export const highlightPanel = [
  {
    title: "Consult",
    desc: "Co-create, design thinking; strengthen infrastructure resist granular. Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress.",
    icon: <ConsultIcon height={30} width={30} fill="#ACAFA9" />,
  },
  {
    title: "Design",
    desc: "Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable.",
    icon: <DesignIcon height={30} width={30} fill="#ACAFA9" />,
  },
  {
    title: "Develop",
    desc: "Revolutionary circular, movements a or impact framework social impact low-hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking.",
    icon: <DevelopIcon height={30} width={30} fill="#ACAFA9" />,
  },
  {
    title: "Marketing",
    desc: "Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective.",
    icon: <MarketingIcon height={30} width={30} fill="#ACAFA9" />,
  },
  {
    title: "Manage",
    desc: "Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social.",
    icon: <ManageIcon height={30} width={30} fill="#ACAFA9" />,
  },
  {
    title: "Evolve",
    desc: "Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer.",
    icon: <EvolveIcon height={30} width={30} fill="#ACAFA9" />,
  },
];
